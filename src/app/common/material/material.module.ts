import { NgModule } from '@angular/core';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';


@NgModule({

imports: [
    MatInputModule,MatButtonModule,MatCardModule,MatFormFieldModule,MatProgressSpinnerModule
],
exports:[
    MatInputModule,MatButtonModule,MatCardModule,MatFormFieldModule
]

})




export class MaterialModule {}