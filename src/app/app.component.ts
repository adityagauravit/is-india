import { Component } from '@angular/core';
import { ActivatedRoute ,Router, Event as RouterEvent, NavigationStart , NavigationEnd, NavigationCancel , NavigationError,  } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'is-india'
  loader:boolean

  constructor(private activatedRoute:ActivatedRoute ,private router: Router){

      router.events.subscribe((event: RouterEvent) => {
          this.navigationInterceptor(event)
      })
  }

  navigationInterceptor(event:RouterEvent){

      if(event instanceof NavigationStart ){
          this.loader = true;
      }

      if(event instanceof NavigationEnd ){
          this.loader = false;
      }

      if(event instanceof NavigationCancel ){
          this.loader = false;
      }

      if(event instanceof NavigationError){
          this.loader =  false;
      }
  }

}
