import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminComponent} from "./admin.component";
import { MaterialModule } from "../common/material/material.module";

import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    MaterialModule,
    FormsModule
  ],
  declarations: [DashboardComponent,AdminComponent]
})
export class AdminModule { }
