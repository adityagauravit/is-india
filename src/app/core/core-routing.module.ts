import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { SearchPageComponent } from '../components/search-page/search-page.component';
import { NotFoundComponent } from "../components/not-found/not-found.component";
import { AdminComponent } from "../admin/admin.component";

const routes: Routes = [

    {
        path: '',
        component: SearchPageComponent
    },

    {
        path: 'home',
        component: SearchPageComponent

    },

    {
        path: 'admin',
        loadChildren:'../admin/admin.module#AdminModule'

    },

    {
        path: '**',
        component:NotFoundComponent
    }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
