import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreRoutingModule } from './core-routing.module';
import { RouterModule } from '@angular/router';

// IMPORT COMPONENTS
import { SearchPageComponent } from '../components/search-page/search-page.component';
import { NotFoundComponent } from '../components/not-found/not-found.component';



//IMPORT DEPENDENCIES
 import { MaterialModule } from '../common/material/material.module';

//IMPORT SERVICES

//IMPORT DIRECTIVES

@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule,
    MaterialModule
  ],
  exports: [
    RouterModule
  ],
  declarations: [SearchPageComponent,NotFoundComponent]
})
export class CoreModule { }
